FROM ubuntu:18.04
 
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
 
# Install tools
RUN apt-get update \
        && apt-get --yes install \
        wget \
        && rm -rf /var/lib/apt/lists/*
 
# Install miniconda
RUN wget --progress=dot:mega https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh \
        && chmod +x miniconda.sh && ./miniconda.sh -b -p /usr/local/conda3 \
        && rm -f miniconda.sh
 
# Set environment PATH
ENV PATH /usr/local/conda3/bin:$PATH
 
# Update configure files
RUN echo 'export PATH=/usr/local/conda3/bin:$PATH' >> /etc/profile.d/pynn.sh \
        && ln -sf /usr/local/conda3/etc/profile.d/conda.sh /etc/profile.d/


RUN python --version

RUN conda update conda

RUN conda install pytorch torchvision cudatoolkit=9.0 -c pytorch

RUN apt-get update

# Install git and clone the scVI repo 
RUN apt-get install -y git

RUN git clone https://github.com/YosefLab/scVI.git
RUN cd scVI && python setup.py install

# Clone the scanpy repo
RUN git clone https://github.com/theislab/scanpy.git
RUN cd scanpy && python -m pip install --upgrade setuptools

# Downgrade scipy
RUN python -m pip install scipy==1.2.0 --upgrade

# Install louvain package
RUN pip install python-louvain

# Install jupyter
RUN conda install jupyter

RUN useradd -ms /bin/bash jupyter

# Change to this new user
USER jupyter

# Set the container working directory to the user home folder
WORKDIR /home/jupyter

# Start the jupyter notebook
ENTRYPOINT ["jupyter", "notebook", "--ip=0.0.0.0", "--no-browser"]


