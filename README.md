# Docker image for running scvi in a jupyter notebook
## How to run

docker pull richamudgal/scvi_scanpy_jupyter

docker run -p 8888:8888 -v $pwd:/home/jupyter richamudgal/scvi_scanpy_jupyter
